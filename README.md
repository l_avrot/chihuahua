# chihuahua

This is an implementation of the Chihuahua challenge (is it a chihuahua or
a muffin?) mostly provided by Boriss Meijias. THis implementation uses python
code to store vectores inside a PostgreSQL database with the pgvector extension.

Steps:

1. Install and run Postgres
2. Install pgvector
2. Create a Postgres database
4. Create the pgvector extension in that database
5. Clone the repo
6. Get the training images from Kaggle
7. Train Postgres using train.py
8. Search for an image from the challenge\_img using search.py
