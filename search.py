#! /usr/bin/env python3

import sys
import matplotlib.pyplot as plt
from PIL import Image
from imgbeddings import imgbeddings
import psycopg2

def main():
    search_img = sys.argv[1]
    schema = sys.argv[2]
    the_host = sys.argv[3] if len(sys.argv) > 3  else "localhost"
    the_port = sys.argv[4] if len(sys.argv) > 4  else "5432"
    the_dbname = sys.argv[5] if len(sys.argv) > 5  else "hackathon"
    the_user = sys.argv[6] if len(sys.argv) > 6  else "postgres"

    # loading the face image path into file_name variable
    file_name = search_img  # replace <INSERT YOUR FACE FILE NAME> with the path to your image
    # opening the image
    img = Image.open(file_name)
    # loading the `imgbeddings`
    ibed = imgbeddings()
    # calculating the embeddings
    embedding = ibed.to_embeddings(img)

    # connecting to the database - replace the SERVICE URI with the service URI
    conn = psycopg2.connect(host=the_host,
                            port=the_port,
                            dbname=the_dbname,
                            user=the_user)

    cur = conn.cursor()
    string_representation = "["+ ",".join(str(x) for x in embedding[0].tolist()) +"]"
    cur.execute("select tag from public.pictures order by embedding <-> %s limit 1", (string_representation,))
    rows = cur.fetchall()
    for row in rows:
        if row[0] == 'muffin':
            print(f"It's a muffin! 🧁")
        else:
            print(f"It's a chihuahua! 🐕")
    cur.close()
    print(sys.argv[0])


if __name__ == '__main__':
    main()
