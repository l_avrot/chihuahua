#! /usr/bin/env python3

import sys
# import cv2 # we don't need to cut faces from images
import numpy as np
from imgbeddings import imgbeddings
from PIL import Image
import psycopg2
import os


def main():
    # arguments
    # 1 - directory where images are stored
    # 2 - schema
    # 3 - tag (chihuahua or muffin)
    # 4 - hostname of the database
    # 5 - port number
    # 6 - database name
    # 7 - username
    source_dir = sys.argv[1]
    schema = sys.argv[2]
    tag = sys.argv[3]
    the_host = sys.argv[4] if len(sys.argv) > 4  else "localhost"
    the_port = sys.argv[5] if len(sys.argv) > 5  else "5432"
    the_dbname = sys.argv[6] if len(sys.argv) > 6  else "hackathon"
    the_user = sys.argv[7] if len(sys.argv) > 7  else "postgres"

    # connecting to the database - replace the SERVICE URI with the service URI
    conn = psycopg2.connect(host=the_host,
                            port=the_port,
                            dbname=the_dbname,
                            user=the_user)

    for filename in os.listdir(source_dir):
        # opening the image
        img = Image.open(f"{source_dir}/{filename}")
        # loading the `imgbeddings`
        ibed = imgbeddings()
        # calculating the embeddings
        embedding = ibed.to_embeddings(img)
        cur = conn.cursor()
        cur.execute(f"insert into {schema}.pictures values (%s,%s,%s)",
                    (filename, embedding[0].tolist(), tag))
        print(filename)
        conn.commit()

    print(sys.argv[0])


if __name__ == '__main__':
    main()
